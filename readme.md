```
  _____  _____  _    _  _______          __     _____       ____ _  _   
 |  __ \|  __ \| |  | |/ ____\ \        / /\   |  __ \     / / /| || |  
 | |  | | |__) | |  | | |  __ \ \  /\  / /  \  | |__) |   / / /_| || |_ 
 | |  | |  _  /| |  | | | |_ | \ \/  \/ / /\ \ |  _  /   / / '_ \__   _|
 | |__| | | \ \| |__| | |__| |  \  /\  / ____ \| | \ \  / /| (_) | | |  
 |_____/|_|  \_\\____/ \_____|   \/  \/_/    \_\_|  \_\/_/  \___/  |_|  
						
             The "Other" Commodies Exchange Game for the C64
	             Copyright 2018 Jay Moore - A FOSS Game
	              http://gitlab.com/dewdude/drugwar64
	              http://gitlab.com/dewdude/drugwar2e
Contents:

About
Instructions
Files
Updates
Support
````
---

About:

Drugwar/64 is a port of Drugwar//e. Here's where it might get
confusing. Drugwar//e is itself an Applesoft BASIC recreation of the 
TI-BASIC (TI83) game DRUGWAR which, itself, was based off a game John
E. Dell wrote for the IBM PC and TRS-80.  The game is based around
the buying and selling of drugs in "a major city". The user has to
contend with constantly changing prices, events designed to cause 
spikes and drops in prices, police trying to catch you, and a loan
shark you owe money. Your goal is to pay off your debt, earn the most
money you can, and make it out alive; all in a period of 30 days. You
are able to buy upgrades and even guns along the way.

Drugwar//e was written in Applesoft BASIC using the help of Loz's 
"Virtual Basic" application. The original TI-BASIC source code was 
used to duplicate game mechanics, logic, and price generation to 
recreate an "authentic" expierence. 

Drugwar//e 1.03 was used as the codebase for the port and the process
lead to me finding a bug in the 1.03 code that had been missed several
times before. The only major changes between 64 1.01 and //e 1.04 are 
how text is displayed; the rest of the "core" code is actually un-
touched. I was actually able to use "Virtual Basic" to generate code
that was 100% C64 compatible after converting it all to lowercase. 
Both because most of the changes were valid syntax in Applesoft, and
the program doesn't actually know the syntax; it largely just numbers
lines and resolves for goto and gosubs. 

The irony of having a C64 port lies in the fact I never owned or used
one in my youth. I knew practically nothing about it other than it was
6502 based with a SID chip and what I'd seen from YouTube videos. In
fact this port was the first and, so far, only game I've actually 
played on a C64.


After the switch to GitLab, I decided to make a second project that was
just each revision submitted in order. BASIC from Alpha 1 up to 1.03 has
been committed; and VirtualBasic from Beta 1 up to 1.03 has also been
committed. It is only there for the curious. 
https://gitlab.com/dewdude/drugwar2ebetas

---

Instructions:

To play Drugwar/64, put the disk in to your 64 and enter LOAD "*",8 or
something. It's the only thing I put on the disk. 

Playing the game is rather simple. Various options on menus have a 
letter emphasised (L)ike (T)his. Merely enter the letter of the option 
you want. For yes or no questions, simply enter Y for yes; the computer
will accept anything else as N. To "back out" of a menu, simply enter
nothing at the prompt. Promps that require you to enter "0" to back
out indicate so in the game. Some interactions, like the police, you
cannot escape out of.
 

Drug prices change every day, and random events can cause them to be
really high or really low. You start out with a debt of $5000 that
you must pay the loan shark; interest on this loan is calculated
every day. The Loan Shark can only be visited in The Bronx. Also in
The Bronx is the bank, where you can deposit money in a savings
account to not only keep it safe; but collect daily interest.
 

Beware, both cops and muggers can be found on the subway. If you deal
too heavily, the police will come after you. Also found on the subway are 
chances to upgrade your abilities by purchasing a larger trechcoat for more
inventory space as well as the ability to buy guns to fight the police. Be 
warned, every gun you buy consumes five inventory slots.

Buy low, sell high...just not on your own supply. Pay off your debt and
earn as much as possible. 

The game ends when you die or complete 30 days.

----

Files:

Your DRUGWAR64.ZIP package contains the following file:

	DRUGWAR64.d64 - Disk image containing DRUGWAR
	drugwar64.bas - Commodore BASIC source
	drugwar64.baz - the Virtual Basic file used to generate the C64 BASIC
	README.md - This readme file
	LICENSE - 2-Clause BSD License (Simplified BSD License)

These files are also available indvidually in the repository. Please not that in accordance with usual
C64 scene behavior; versions are also available outside of the repository. These disk images have the
version number in the filename. Images download from the repository share the same name and are always
updated. Older versions may be available from the repository as well.
	
---
	
Updates - See CHANGELOG for complete details.

5-NOV-2018: Version 1.02

	Changed license to Simplified BSD License
	Added random color border since I couldn't decide on text coloring.
	Code Condensing:
		Removed all REM statements
		Combined lines to reduce from 749 to just over 400.
	Fixed the gaping cheat hole
		Added some special anti-cheat stuff to let you cheat a little.
		Can also punish you and boot you from game.
	Police shoot at you when you run.
	Changed how inventory affects cops
		It's now half freespace vs just 50. 
	Changed how health is handled
		Starts at 100, goes down to 0. Police do 2x damage to compensate.
	Removed rogue "CLEAR" from Applesoft port.
	
29-OCT-2018: Version 1.01

	Inital release of C64 Port. Based off Drugwar//e 1.03 code.
	
	Please see Drugwar//e readme for updates to the core logic.
	
---
	
Support
	
	If you encounter problems, you can find me on CSDb as dewdude
	as well as the Lemon64 forum as dewdude. my username at gmail
	works as well. So does Twitter, I'm _NQ4.
	

	
	
